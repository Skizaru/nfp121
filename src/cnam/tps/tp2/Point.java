package cnam.tps.tp2;

public class Point {
    private Double x = 0.0;
    private Double y = 0.0;


    public Point(double xPos, double yPos) {
        this.setX(xPos);
        this.setY(yPos);
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public void setX(double newX) {
        this.x = newX;
    }

    public void setY(double newY) {
        this.y = newY;
    }

    public void translateCart(double xMove, double yMove) {
        this.x = x + xMove;
        this.y = y + yMove;
    }

    public void setPositionCart(double xPos, double yPos) {
        this.x = xPos;
        this.y = yPos;
    }

    public Double getModule() {
        return Math.sqrt(this.x + this.y);
    }

    public Double getArgument() {
        return Math.atan(this.y / this.x);
    }

    public double distance(Point target) {
        return Math.sqrt(((target.x - this.x) * (target.x - this.x)) + ((target.y - this.y) * (target.y - this.y)));
    }

    public static Point calculCenterPointBetweenTwoPoints(Point p1, Point p2) {
        Double valX = (p1.getX() + p2.getX())/2;
        Double valY = (p1.getY() + p2.getY())/2;
        Point centerPoint = new Point(valX, valY);
        return centerPoint;
    }

    public String toString() {
        return "Point(" + this.x.toString() + ", " + this.y.toString() + ")";
    }
}