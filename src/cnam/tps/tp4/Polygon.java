package cnam.tps.tp4;

import cnam.tps.tp2.Point;

public class Polygon {
    private Point[] points;
    private int degree;

    public Polygon(Point[] p) {
        this.points = p;
        this.degree = this.points.length;
    }

    public void addVertex(Point p) {
        Point[] points = new Point[this.points.length+1];
        for(int i=0; i < this.points.length; i++) {
            points[i] = this.points[i];
        }
        points[this.points.length+1]=p;
        this.points = points;
        this.degree=this.degree+1;
    }

    public void addVertex(Point p, int index) {
        Point[] points = new Point[this.points.length+1];
        for (int i = 0, k = 0; i < this.points.length+1; i++) {
            if (i == index) {
                points[k++]=p;
            } else {
                points[k++] = this.points[i];
            }
        }
        this.points = points;
        this.degree=this.degree+1;
    }

    public Point getVertex(int i) {
        return this.points[i-1];
    }

    public Point[] getPoints() {
        return points;
    }

    public void setPoints(Point[] points) {
        this.points = points;
    }

    public int getDegree() {
        return this.degree;
    }

    public double getPerimeter() {
        Double p = 0.0;
        for (int i=0; i<this.points.length-1; i++) {
            p += Math.sqrt((this.points[i+1].getX()-this.points[i].getX())*(this.points[i+1].getX()-this.points[i].getX())+
                    (this.points[i+1].getY()-this.points[i].getY())*(this.points[i+1].getY()-this.points[i].getY()));
        }
        p+=Math.sqrt((this.points[0].getX()-this.points[this.points.length].getX())*(this.points[0].getX()-this.points[this.points.length].getX())+
                (this.points[0].getY()-this.points[this.points.length].getY())*(this.points[0].getY()-this.points[this.points.length].getY()));
        return p;
    }

    public double getArea() {
        double sum = 0;
        for (int i = 0; i < this.points.length ; i++) {
            if (i == 0) {
                System.out.println(this.points[i].getX() + "x" + (this.points[i + 1].getY() + "-" + this.points[this.points.length - 1].getY()));
                sum += this.points[i].getX() * (this.points[i + 1].getY() - this.points[this.points.length - 1].getY());
            } else if (i == this.points.length - 1) {
                System.out.println(this.points[i].getX() + "x" + (this.points[0].getY() + "-" + this.points[i - 1].getY()));
                sum += this.points[i].getX() * (this.points[0].getY() - this.points[i - 1].getY());
            } else {
                System.out.println(this.points[i].getX() + "x" + (this.points[i + 1].getY() + "-" + this.points[i - 1].getY()));
                sum += this.points[i].getX() * (this.points[i + 1].getY() - this.points[i - 1].getY());
            }
        }
        double area = 0.5 * Math.abs(sum);
        return area;
    }

    public void removeVertex(int index) {
        Point[] anotherarray = new Point[this.points.length - 1];
        for (int i = 0, k = 0; i < this.points.length; i++) {
            if (i == index) {
                continue;
            }
            anotherarray[k++] = this.points[i];
        }
        this.points = anotherarray;
    }

    public void translate(int x, int y) {
        for (int i=0; i<this.points.length-1; i++) {
            this.points[i].setY(this.points[i].getY()+y);
            this.points[i].setX(this.points[i].getX()+x);
        }
    }
}