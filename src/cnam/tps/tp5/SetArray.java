package cnam.tps.tp5;

import java.lang.reflect.Array;
import java.util.Arrays;

public class SetArray implements Set {
    private int[] setArray;

    public SetArray(int size) {
        this.setArray = new int[size];
    }
    @Override
    public int cardinal() {
        return this.setArray.length;
    }

    @Override
    public boolean isEmpty() {
        if (this.setArray.length == 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(int e) {
        for(int i=0; i < this.cardinal() - 1; i++) {
            if (this.setArray[i] == e) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void add(int e) {
        if (!this.contains(e)) {
            int newSetArray[] = new int[this.cardinal() + 1];
            for (int i = 0; i < this.cardinal() - 1; i++) {
                newSetArray[i] = this.setArray[i];
            }
            newSetArray[this.cardinal()] = e;
            this.setArray = newSetArray;
        }
    }

    @Override
    public void remove(int e) {
        if (this.contains(e)) {
            int newSetArray[] = new int[this.cardinal() - 1];
            int counter = 0;
            for (int i = 0; i < this.cardinal() - 1; i++) {
                if (this.setArray[i] != e) {
                    newSetArray[counter] = this.setArray[i];
                    counter++;
                }
            }
            this.setArray = newSetArray;
        }
    }

    @Override
    public int min() {
        if (!this.isEmpty()) {
            int min = this.setArray[0];
            for (int i = 1 ; i < this.cardinal() - 1 ; i++) {
                if (this.setArray[i] < min) {
                    min = this.setArray[i];
                }
            }
            return min;
        }
        return 0;
    }
}
