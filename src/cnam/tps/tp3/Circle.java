package cnam.tps.tp3;

import cnam.tps.tp2.Point;

public class Circle {
    private Point center;
    private Double radius;
    private EnumColor color;

    /**
     * @param center Point representant le center du cercle
     * @param radius Un double representant le radius
     * @param color C'est une color
     **/
    public Circle(Point center, double radius, EnumColor color) {
        this.setCenter(center);
        this.setRadius(radius);
        this.setColor(color);
    }

    public Circle(Point center, double radius) { //11
        EnumColor newColor = EnumColor.GREEN;
        this.setCenter(center);
        this.setRadius(radius);
        this.setColor(newColor);
    }

    public Circle(Point p1, Point p2) { //12
        this.center = Point.calculCenterPointBetweenTwoPoints(p1, p2);
        this.radius = this.center.distance(p2);
    }
    public Circle(Point p1, Point p2, EnumColor color) { //13
        this.center = Point.calculCenterPointBetweenTwoPoints(p1, p2);
        this.radius = this.center.distance(p2);
        this.setColor(color);
    }

    public static Circle createCircle(Point center, Point circ) { //14
        Double radius = center.distance(circ);
        Circle newCircle = new Circle(center, radius);
        return newCircle;
    }

    public void setCenter(Point newCenter) {
        this.center = newCenter;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void setDiametre(double diametre) { // 17
        this.radius = diametre/2.0;
    }

    public void setColor(EnumColor color) {
        this.color = color;
    }

    public String getColor() {
        return this.color.toString();
    }

    //1
    public void translateX(double newX) {
        this.center.setX(newX);
    }

    public void translateY(double newY) {
        this.center.setY(newY);
    }

    //2
    public Point getCenter() {
        return this.center;
    }

    //3
    public double getRadius() {
        return this.radius;
    }

    //4
    public double getDiametre() {
        return this.radius * 2.0;
    }

    // 5
    public boolean contains(Point p1) {
        return Math.pow((p1.getX() - this.center.getX()), 2) + Math.pow((p1.getY() - this.center.getY()), 2) < Math.pow(this.radius, 2);
    }

    // 6
    public double getPerimeter() {
        return 2.0 * Math.PI * this.radius;
    }

    public double getAire() {
        return (Math.PI * this.radius)*(Math.PI * this.radius);
    }

    //7
    //8
    public String toString() { //15
        //"Cr@(a, b)"
        return "C" + this.radius.toString() + this.center.toString();
    }

    // 18 Tout est private
}