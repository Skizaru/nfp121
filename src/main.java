import cnam.tps.tp3.Circle;
import cnam.tps.tp2.Point;

public class main {
    public static void main(String[] args) {
        // 3
        Point p3 = new Point(1.0, 0.0);
        System.out.println("Module : " + p3.getModule() + " - Abscisse : " + p3.getY());
        p3.translateCart(-2.0, 1.0); // Translate jusqu'en (-1, 1)
        System.out.println(p3.toString());

        // 4
        Point p4 = new Point(1.0, 0.0);
        p4.setX(10.0);
        System.out.println(p4.getModule());

        // Cercle
        Circle c1 = new Circle(p3, 2.0);
        System.out.println(c1.toString());
    }
}